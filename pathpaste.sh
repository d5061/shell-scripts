#!/bin/bash
#This is a script for simply pasting a string, that makes it easier to include images in LaTeX files.
TEXT="~/Documents/LaTeX/images/"
printf '%s' "$TEXT" | xclip -i -selection clipboard
xdotool sleep 0.2 key --clearmodifiers "ctrl+v"
