#!/bin/bash
#file=$1
n_of_tests=$1
> test.results
for i in $(seq 1 $n_of_tests)
do
    cargo run >> test.results
done
python3 $HOME/Programming/Bash_Scripts/rustbench/average.py test.results >> average
