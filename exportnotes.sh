#!/bin/bash
# This script is meant to be used alongside texnote.sh to collect all of the pdfs created in lecture folders. 
# The script should be executed while the working directory is the parent directory of lecture folders.

base_dir="$(pwd)"
mkdir PDFs > /dev/null
for directory in $(exa -D -I=PDFs)
do
    pushd $directory > /dev/null
    for target in $(exa -l | awk '$7 ~ /(.pdf)/ {print $7}')
    do
      mv $target $base_dir/PDFs/
    done
    popd > /dev/null
done
